#Nefarious
>- Requirements
>- Installation
>- Credits
----
###Requirements
>- Linux-based distro with Python 2.7
>- amixer (installed by default)
>- notify-send (installed by default)

**If you do not have notify-send installed by default open a terminal window and type**

	sudo apt-get install notify-send #for Debian/Ubuntu

**Note that the default script comes with "card1" so open a terminal window and type**

	amixer -c0 contents | grep -A 2 'Headphone Jack'
	amixer -c1 contents | grep -A 2 'Headphone Jack'
**If the first command does not return an output use the second and remember to change the command in /usr/local/bin/nefarious/nefarious line 8**
### Installation

	git clone https://github.com/dr34mwh1t3/nefarious/nefarious.git
	sudo cp -r nefarious /usr/local/bin/
#####Then go in "Startup Applications" and add the Nefarious Audio Manager with the script location in /usr/local/share/nefarious/nefarious
	

###Credits
[Dreamwhite](https://telegram.me/dr34mwh1te) (main developer)
